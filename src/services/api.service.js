import axios from 'axios';
axios.defaults.baseURL = process.env.APP_ROOT_API;

export default {

    getSettings() {
    	return axios.get("options.json").then(response =>{
    		return response;
    	});
    	
    }
}