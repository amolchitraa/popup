// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios';
import vueCustomElement from 'vue-custom-element';
import 'document-register-element/build/document-register-element';
import './assets/css/style.css';
import './assets/css/icomoon.css';

Vue.config.productionTip = false
Vue.use(vueCustomElement);

//App.router = router
Vue.customElement('vue-popup', App)

/* eslint-disable no-new */
/*new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})*/
