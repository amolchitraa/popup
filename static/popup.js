(function (global) {

    //global paths
    //var popup_root = process.env.POPUP_ROOT_URI;
    var popup_root = 'http://67.20.105.42/vue-popup/popup/static/';

    // make a global object to store stuff in
    if(!global.popupWidget) { global.popupWidget = {}; };
    var popupWidget = global.popupWidget;

    var vdate = new Date();
    var vtime = vdate.getTime();

    // To keep track of which embeds we have already processed
    if(!popupWidget.processedScripts) { popupWidget.processedScripts = []; };
    var processedScripts = popupWidget.processedScripts;
    var scriptTags = document.getElementsByTagName('script');

    for(var i = 0; i < scriptTags.length; i++) {

        var scriptTag = scriptTags[i];
        if ( scriptTag.id == 'popup_snippet' ) {

            processedScripts.push(scriptTag);
            // Create a Button
            var wArea = document.createElement('iframe');
            wArea.id = 'popupArea';
            wArea.scrolling= 'no';
            wArea.allowTransparency='true';
            wArea.frameBorder='0';
            wArea.style.background = 'transparent';
            wArea.style.border = 'none';
            wArea.style.position = 'fixed';
            wArea.style.zIndex = '999999';
            wArea.style.width = '100%';
            wArea.style.height = '100%';
            wArea.style.maxWidth = '560px';
            wArea.style.maxHeight = '560px';
            wArea.style.top = '50%';
            wArea.style.left = '0';
            wArea.style.right = '0';
            wArea.style.margin = '-275px auto 0 auto';

            document.body.appendChild(wArea);

            //add js & css from dist
            var cssApp =  popup_root + 'css/app.css?v=' + vtime;
            var jsApp =  popup_root + 'js/app.js?v=' + vtime;
            var jsMani =  popup_root + 'js/manifest.js?v=' + vtime;
            var jsVen =  popup_root + 'js/vendor.js?v=' + vtime;

            var iframeHTML = "<!doctype html><head><meta name=viewport content='width=device-width,initial-scale=1'><link href='"+ cssApp +"' rel='stylesheet' /></head><body><vue-popup title='Vue Popup'></vue-popup><script type='text/javascript' src='"+ jsMani +"'></script><script type='text/javascript' src='"+ jsVen +"'></script><script type='text/javascript' src='"+ jsApp +"'></script></body></html>";
            wArea.contentWindow.document.write(iframeHTML);

        }/* end : if */
    }

})(this);